# Vue.js Todo List

This is a sample Vue.js project for my POS technical document.

# Getting started

To get started, install all necessary dependencies using
``` bash
npm install
```

To serve the application (localhost:8080) enter
``` bash
npm run dev
```
