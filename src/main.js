import Vue from 'vue'
import App from './App'
import VueRouter from 'vue-router'
import TodoList from './components/TodoList'
import LandingPage from './components/LandingPage'

Vue.config.productionTip = false
Vue.use(VueRouter)

const routes = [
  {path: '/todo-list', name: 'todo-list', component: TodoList},
  {path: '/', name: 'home', component: LandingPage}
]

const router = new VueRouter({
  routes
})

new Vue({
  el: '#app',
  router: router,
  components: { App },
  template: '<App/>'
})


